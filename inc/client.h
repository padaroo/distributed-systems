#ifndef CLIENT_H
#define CLIENT_H

#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <unistd.h>
#include <pthread.h>
#include <strings.h>
#include <ncurses.h>

#define MAXDATASIZE 100 /* max number of bytes we can get at once */
#define ARRAY_SIZE 30
#define RETURNED_ERROR -1
#define RANDOM_NUMBER_SEED 42
#define NUM_TILES_X 9 + 2
#define NUM_TILES_Y 9 + 2
#define NUM_MINES 10
#define CURSOR_OFFSET 2

#define h_addr h_addr_list[0] /* for backward compatibility */

/* store game state in a struct */
typedef struct GameState
{
  char tiles[NUM_TILES_X][NUM_TILES_Y];  /* we only share the char symbol of the tiles */
  int mines_remaining;
  bool running; 		/* is the game running? */
  int cursor_x;
  int cursor_y;
  int mine_counter; 		/* Number of mines left */
  char *a_mine;			/* Used for game display / info information */
  bool over;			/* is the game over? */
} Mine_Yard;


/* Function Declarations */
void init_ncurses();
void Send_Array_Data(int socket_id, int *myArray);
void final_tiles(int socket_id, Mine_Yard *game, WINDOW *game_window);
int* Receive_Array_Int_Data(int socket_identifier, int size);
void show_tiles(Mine_Yard *game,  WINDOW *window);
void render_game(Mine_Yard *game, WINDOW *info_window, WINDOW *game_window);
void display_lb(int socket_id);
void init_mines(Mine_Yard *game);
int reveal_tile(int socket_id, Mine_Yard *game);
void place_flag(int socket_id, Mine_Yard *game);
int run_game(int socket_id);
void main_menu(int socket_id);
void shut_down(int socket_id);
void authenticate(int sockfd);

#endif /* CLIENT_H */
