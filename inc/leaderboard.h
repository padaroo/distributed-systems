#ifndef LEADERBOARD_H
#define LEADERBOARD_H

#include <stdlib.h>
#include <stdio.h> 
#include <errno.h> 
#include <string.h>

/*   
* Linked-list types.
*/
typedef struct NODE_PLAYER_S
{
  /* Data Payload (defined by player) */
  char name[10];
  int time;
  int won; // games won
  int num_games_played;
} NODE_PLAYER_T;

typedef struct LIST_NODE_S
{
  /* Next-node pointer */
  struct LIST_NODE_S *next;     /* pointer to the next node in the list. */
  NODE_PLAYER_T      payload;  /* Data Payload (Player info) */
} LIST_NODE_T;

void swap(LIST_NODE_T *a, LIST_NODE_T *b);
void bubble_sort(LIST_NODE_T *head);
int Insert_Node(LIST_NODE_T **IO_head,
		          char *I__name,
			  int I__time,
			  int I__won,
			  int I__num_games_played);
int OrderNodeByName(LIST_NODE_T *I__head,
			       const char  *I__name,
			       LIST_NODE_T **_O_parent
			       );
#endif	/* LEADERBOARD_H */
