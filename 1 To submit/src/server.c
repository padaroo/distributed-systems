#include "../inc/server.h"

/* For graceful exit use:  C-c */
void INThandler(int sig) {
  signal(sig, SIG_IGN);
  exit(EXIT_SUCCESS);
}

/*
 * function add_request(): add a request to the requests list
 * algorithm: creates a request structure, adds to the list, and
 *            increases number of pending requests by one.
 * input:     request number, linked list mutex.
 * output:    none.
 */
void add_request(int socket_id,
		 pthread_mutex_t* p_mutex,
		 pthread_cond_t*  p_cond_var)
{
  struct request* a_request;      /* pointer to newly added request.     */

  /* create structure with new request */
  a_request = (struct request*)malloc(sizeof(struct request));
  if (!a_request) { /* malloc failed?? */
    fprintf(stderr, "add_request: out of memory\n");
    exit(1);
  }
  a_request->socket_id = socket_id;
  a_request->next = NULL;

  /* lock the mutex, to assure exclusive access to the list */
  pthread_mutex_lock(p_mutex);

  /* add new request to the end of the list, updating list */
  /* pointers as required */
  if (num_requests == 0) { /* special case - list is empty */
    requests = a_request;
    last_request = a_request;
  }
  else {
    last_request->next = a_request;
    last_request = a_request;
  }

  /* increase total number of pending requests by one. */
  num_requests++;

  /* unlock mutex */
  pthread_mutex_unlock(p_mutex);

  /* signal the condition variable - there's a new request to handle */
  pthread_cond_signal(p_cond_var);
}

/*
 * function get_request(): gets the first pending request from the requests list
 *                         removing it from the list.
 * algorithm: creates a request structure, adds to the list, and
 *            increases number of pending requests by one.
 * input:     request number, linked list mutex.
 * output:    pointer to the removed request, or NULL if none.
 * memory:    the returned request need to be freed by the caller.
 */
struct request* get_request(pthread_mutex_t* p_mutex)
{
  struct request* a_request;      /* pointer to request.                 */

  /* lock the mutex, to assure exclusive access to the list */
  pthread_mutex_lock(p_mutex);

  if (num_requests > 0) {
    a_request = requests;
    requests = a_request->next;
    if (requests == NULL) { /* this was the last request on the list */
      last_request = NULL;
    }
    /* decrease the total number of pending requests */
    num_requests--;
  }
  else { /* requests list is empty */
    a_request = NULL;
  }

  /* unlock mutex */
  pthread_mutex_unlock(p_mutex);

  /* return the request to the caller. */
  return a_request;
}

/*
 * function handle_request(): handle a single given request.
 * Starts the authentication for a given client on a separate thread
 */
void handle_request(int socket_id)
{
  
  /* New struct for player details */
  PLAYER_T Player;
    
  for (;;) {
    
    bzero(buffer, 256); 	/* Clear the user_name buffer */
    /* Get user name -- exit if socket is non-existent*/
    if (recv(socket_id, buffer, MAXDATASIZE, 0) <= 0){
      close(socket_id);
      return;
    }

    char line[100];  /* to read the file line by line */

    /* 1. Open Authentication file*/
    FILE *fp;
    fp=fopen("./Authentication.txt", "r");
    if (fp == NULL) {
      fprintf(stderr, "Could not open file, %s", strerror(errno));
    }

    /* Update player details */
    strcpy(Player.name, buffer);
    char user_name[12], password[12];

    /* Check default Username and Password is not attempted */
    if (!strncmp(buffer, "Username", MAXDATASIZE)) {
      close(sockfd);
      return;
    }
	
    /* 2. Scan each line of file */
    while (fgets(line, sizeof(line), fp) != NULL) {

      /* 3. Check user name */
      sscanf(line, "%s%s", user_name, password);

      /* User name is found */
      if ((strcmp(buffer, user_name)) == 0) {
	bzero(buffer, 256);
	  
	/* Read in password to buffer */
	n = read(socket_id, buffer, 255);
	if (n < 0) perror("ERROR reading from socket");
	if (n == 0) {
	  close(socket_id);
	  return;
	}

	/* 4. Check Password */
	if ((strcmp(buffer, password)) == 0) {
	  ans = 'y';
	  bzero(user_name, 40);
	  bzero(password, 40);
	}
      }
    }

    /* 6. Close the authentication file */
    fclose(fp);
      
    n = write(socket_id,&ans,1);
      
    if (ans != 'y') {
      close(socket_id);
    }
      
    else {
      ans = 'n'; 		/* reset so that auth can be done again*/
      Player.wins = 0; // Initial player info
      Player.games_played = 0;
      Client_Request(socket_id, Player);
    }
  }
  close(socket_id);
}

/*
 * function handle_requests_loop(): infinite loop of requests handling
 * algorithm: forever, if there are requests to handle, take the first
 *            and handle it. Then wait on the given condition variable,
 *            and when it is signaled, re-do the loop.
 *            increases number of pending requests by one.
 * output:    none.
 */
void* handle_requests_loop(void *data)
{
  struct request* a_request; 
  int thread_id = *((int *)data);
  /* lock the mutex, to access the requests list exclusively. */
  pthread_mutex_lock(&request_mutex);

  /* do forever.... */
  while (1) {

    if (num_requests > 0) { /* a request is pending */
      a_request = get_request(&request_mutex);
      if (a_request) { /* got a request - handle it and free it */
         
	pthread_mutex_unlock(&request_mutex);
	int socket_id = a_request->socket_id;
	printf("Thread %d is handling a request for socket %d\n", thread_id, socket_id);
	handle_request(socket_id);
	free(a_request);
	close(socket_id);
	printf("Thread %d has finished a request for socket %d\n", thread_id, socket_id);
	pthread_mutex_lock(&request_mutex);
      }
    }
    else {
      /* wait for a request to arrive. note the mutex will be */
      /* unlocked here, thus allowing other threads access to */
      /* requests list.                                       */

      pthread_cond_wait(&got_request, &request_mutex);
      /* and after we return from pthread_cond_wait, the mutex  */
      /* is locked again, so we don't need to lock it ourselves */

    }
  }
}

/* Send an int or an array of ints */
void Send_Array_Data_Int(int socket_id, int *myArray) {
  int i=0;
  uint16_t statistics;  
  for (i = 0; i < ARRAY_SIZE; i++) {
    statistics = htons(myArray[i]);
    send(socket_id, &statistics, sizeof(uint16_t), 0);
  }
}

/* Send the leaderboard to the client */
/* Mutex locks used for the shuffling of it */
int send_lb(LIST_NODE_T *head, int socket_id)
{
  int rCode=0;
  LIST_NODE_T *cur = head;
  int nodeCnt=0;
  int num_nodes = 0;
  /* Send name, time, won and games played */
  
  while (cur) {
    ++num_nodes;
    cur = cur -> next;
  }

  cur = head;
  
  Send_Array_Data_Int(socket_id, &num_nodes);

  /* Suffle the leaderboard */
  pthread_mutex_lock(&request_mutex);
  bubble_sort(lboard);
  pthread_mutex_unlock(&request_mutex);


  while(cur)
    {
      ++nodeCnt;
      send(socket_id, cur->payload.name, ARRAY_SIZE, 0);
      Send_Array_Data_Int(socket_id, &cur->payload.time);
      Send_Array_Data_Int(socket_id, &cur->payload.won);
      Send_Array_Data_Int(socket_id, &cur->payload.num_games_played);
      cur=cur->next;
    }
  return(rCode);
}


/* send all the symbol from the mineyard to the client*/
void SendMineYard(int socket_id, Mine_Yard *game){
  for (int i = 0; i < NUM_TILES_Y; i++) {
    for (int j = 0; j < NUM_TILES_X; j++) {
      n = write(socket_id,&game->tiles[i][j].symbol,1); 
      if (n < 0) perror("ERROR writing to socket");
    }
  }
}

/* init the number of adjacent mines*/
void init_adjacent_mines (Mine_Yard *minesweeper){
  for(int i = 0; i<NUM_TILES_X; i++){
    for(int j = 0; j<NUM_TILES_Y; j++){
      for(int x = i-1; x<i+2;x++){
	for(int y = j-1; y<j+2;y++){
	  if(minesweeper->tiles[x][y].is_mine==true){
	    minesweeper->tiles[i][j].adjacent_mines = minesweeper->tiles[i][j].adjacent_mines +1;
	  }
	}
      }
    }
  }
}

/* resets all tiles to unknown and then populates 10 mines */
void clear_tiles(Mine_Yard *minesweeper) {
  int r, c;
  /* set inital variables to all tiles */
  for (r = 0; r < NUM_TILES_Y; r++)
    {
      for (c = 0 ; c < NUM_TILES_X; c++)
	{
	  if (r == 0 || r >= NUM_TILES_Y-1 || c == 0 || c >= NUM_TILES_X-1) {
	    minesweeper->tiles[r][c].symbol = '.';
	    minesweeper->tiles[r][c].revealed = false;
	    minesweeper->tiles[r][c].is_mine = false;
	    minesweeper->tiles[r][c].exists = false;
	    minesweeper->tiles[r][c].y_coord = r;
	    minesweeper->tiles[r][c].x_coord = c;
	    minesweeper->tiles[r][c].adjacent_mines = 0;
	  }
	  else {
	    minesweeper->tiles[r][c].symbol = '-';
	    minesweeper->tiles[r][c].is_mine = false;
	    minesweeper->tiles[r][c].revealed = false;
	    minesweeper->tiles[r][c].exists = true;
	    minesweeper->tiles[r][c].y_coord = r;
	    minesweeper->tiles[r][c].x_coord = c;
 	    minesweeper->tiles[r][c].adjacent_mines = 0;
	  }
	}
    }
}

/* resets all tiles to unknown and then populates 10 mines */
void init_tiles(Mine_Yard *minesweeper) {
  int r, c;
  clear_tiles(minesweeper);
  
  /* set inital variables to all tiles */
  for (r = 0; r < NUM_TILES_Y; r++)
    {
      for (c = 0 ; c < NUM_TILES_X; c++)
	{
	  if (r == 0 || r >= NUM_TILES_Y-1 || c == 0 || c >= NUM_TILES_X-1) {
	    minesweeper->tiles[r][c].symbol = '.';
	    minesweeper->tiles[r][c].revealed = false;
	    minesweeper->tiles[r][c].is_mine = false;
	    minesweeper->tiles[r][c].exists = false;
	    minesweeper->tiles[r][c].y_coord = r;
	    minesweeper->tiles[r][c].x_coord = c;
	  }
	  else {
	    minesweeper->tiles[r][c].symbol = '-';
	    minesweeper->tiles[r][c].is_mine = false;
	    minesweeper->tiles[r][c].revealed = false;
	    minesweeper->tiles[r][c].exists = true;
	    minesweeper->tiles[r][c].y_coord = r;
	    minesweeper->tiles[r][c].x_coord = c;
	  }
	}
    }
}

void add_mines(Mine_Yard *minesweeper) {
  minesweeper->mines_remaining = NUM_MINES;
  minesweeper->mine_counter = 0;

  /* add 10 mines randomly */
  int i;
  for (i = 0; i < NUM_MINES; i++)
    {
      int x, y;
      do
	{
	  /* Making random thread safe */
	  pthread_mutex_lock(&request_mutex);
	  x = (rand() % NUM_TILES_X-1);
	  y = (rand() % NUM_TILES_Y-1);
	  pthread_mutex_unlock(&request_mutex);
	}
      while (minesweeper->tiles[y][x].is_mine || !minesweeper->tiles[y][x].exists);
      /* place mine at x,y */
      minesweeper->tiles[y][x].is_mine = true;
      minesweeper->mine_counter++;
    }
}

/* Changes symbol from int to char for eventual client
   use. */
void change_symbol(int x, int y, Mine_Yard *game){
  
  if(game->tiles[x][y].adjacent_mines == 0){
    game->tiles[x][y].symbol = '0';
  } 
  else if(game->tiles[x][y].adjacent_mines == 1){
    game->tiles[x][y].symbol = '1';
  } 
  else if(game->tiles[x][y].adjacent_mines == 2){
    game->tiles[x][y].symbol = '2';
  } 
  else if(game->tiles[x][y].adjacent_mines == 3){
    game->tiles[x][y].symbol = '3';
  } 

  else if(game->tiles[x][y].adjacent_mines == 4){
    game->tiles[x][y].symbol = '4';
  } 	
}


//if adjacent tiles are 0 then reveale the other tiles around
void reveal(int x, int y, Mine_Yard *game){
  game->tiles[x][y].revealed =true;
  change_symbol(x,y,game);
  if(game->tiles[x][y].adjacent_mines==0){
    for(int i = x-1; i<x+2;i++){ 
      for(int j = y-1; j<y+2;j++){ 
	if(x!=i || y!=j){
  	  if(game->tiles[i][j].revealed ==false && game->tiles[i][j].exists ==true){
	    reveal(i,j,game); 
	  }
	}
      }
    }
  }
}


/*get the tile coordinate from the client
  if its a mine game over
  if not revealed it and chage symbol
  if adjacent tiles ==0 then revealed the other tiles around
  At the end send back the mineyard to the client for him to display*/
int reveal_tile(int socket_id, Mine_Yard *game){
  int *y = Receive_Array_Int_Data(socket_id,  ARRAY_SIZE);
  int *x = Receive_Array_Int_Data(socket_id,  ARRAY_SIZE);
  *x = (*x)-2;
  *y = (*y)-2;
  
  //game->tiles[*x][*y].is_mine = true; // remove later
  //if: there is a mine - send to client and go back to menu
  if(game->tiles[*x][*y].is_mine){
    game->tiles[*x][*y].symbol = '*';
    //send to the client that there is a mine here
    ans = 'y';
    n = write(socket_id,&ans,1);
    if (n < 0) {
      perror("ERROR writing to socket");
    }
    return 1;
  }

  //else: send to the client that there is a no mine there
  ans = 'n';
  n = write(socket_id,&ans,1); /*Tell the client there is no mine here */
  if (n < 0){
    perror("ERROR writing to socket");
  }
  reveal(*x,*y,game);
  SendMineYard(socket_id,game);
  return 0;
}


int place_flag(int socket_id, Mine_Yard *game){
  int *y = Receive_Array_Int_Data(socket_id,  ARRAY_SIZE);
  int *x = Receive_Array_Int_Data(socket_id,  ARRAY_SIZE);
  *x = (*x)-2; // account for client offsets
  *y = (*y)-2;

  if(game->tiles[*x][*y].is_mine){
    game->tiles[*x][*y].symbol = '+';
    
    //send to the client that there is a mine here
    ans = 'y';
    n = write(socket_id,&ans,1);

    if (n < 0) perror("ERROR writing to socket");
    
    game->mines_remaining --;
  }

  else {
    ans = 'n';
    n = write(socket_id,&ans,1);
    if (n < 0) perror("ERROR writing to socket");
  }

  SendMineYard(socket_id,game);
  return 0;
}

/* For end of game display, send all the tiles back to the client */
void reveal_all_tiles(int socket_id, Mine_Yard *game){
  for(int x = 0; x<NUM_TILES_X; x++){
    for(int y = 0; y<NUM_TILES_Y; y++){
      change_symbol(x,y,game);
      if(game->tiles[x][y].is_mine){
	game->tiles[x][y].symbol = '*';
      }
    }
  }
  SendMineYard(socket_id,game);	
}

// Initialize and run the game
void play_minesweeper(int socket_id, PLAYER_T *Player){
  Mine_Yard game;
  init_tiles(&game);
  add_mines(&game);
  init_adjacent_mines(&game); 
  game.running=true;
  
  /* Start the timer */
  time_t start_t, end_t;
  int diff_t;
  time(&start_t);

  int exit = 0;
  
  while(game.running){
    int *r = Receive_Array_Int_Data(socket_id,  ARRAY_SIZE);
    if (*r > 0) {

      /* ascii values used here */
      switch (*r) {
	
      case 114:			/* ASCII for Reveal tile -r */
	exit = reveal_tile(socket_id,&game);
	if (exit) {
	  Player->games_played++;
	  reveal_all_tiles(socket_id, &game);
	  game.running = false;
	}
	break;

      case 112:			/* Place flag - 'p' */
	place_flag(socket_id, &game);
	if(game.mines_remaining == 0) {
	  reveal_all_tiles(socket_id, &game);
	  Player->wins++;
	  Player->games_played++;
	  time(&end_t);
	  diff_t = difftime(end_t, start_t);
	  pthread_mutex_lock(&request_mutex);
	  Insert_Node(&lboard, Player->name, diff_t, Player->wins, Player->games_played);
	  pthread_mutex_unlock(&request_mutex);
	  game.running = false;
	}
     	break;
	
      case 113:			/* Quit - 'q' */
	Player->games_played++;
	game.running = false;
	break;
      }
    }
  }
  return; 
}

/* Receives an array of int data of any size and returns the result as a pointer to an int */
int* Receive_Array_Int_Data(int socket_identifier, int size) {
  int number_of_bytes, i=0;
  uint16_t statistics;
	
  int *results = malloc(sizeof(int)*size);
  for (i=0; i < size; i++) {
    if ((number_of_bytes=recv(socket_identifier, &statistics, sizeof(uint16_t), 0))
	== RETURNED_ERROR) {
      perror("recv");
      exit(EXIT_FAILURE);			
    }
    results[i] = ntohs(statistics);
  }
  return results;
}

/* main client request this function is called after  successful authentication */
/* Player T is a struct containing player name and game info */
void Client_Request(int socket_id, PLAYER_T Player){

  for (;;){
    /* Receive client's request for game, leaderbord or quit */
    int *r = Receive_Array_Int_Data(socket_id,  ARRAY_SIZE);
    if (*r < 0) {
      perror("ERROR writing to socket");
    }
    
    else if (*r > 0) {      
      switch (*r) {
      case 3: // exit
	return;
      case 2: // show leaderboard
	send_lb(lboard, socket_id);
	break;
      case 1: // play
	play_minesweeper(socket_id, &Player);
	break;
      }
    }
  }
}



int main(int argc, char *argv[]) {
  
  /* For graceful C-c exits */
  signal(SIGINT, INThandler);

  /* Seed the random number generator */
  srand(RANDOM_NUMBER_SEED);

  int sockfd, new_fd, port;  /* listen on sock_fd, new connection on new_fd */
  struct sockaddr_in my_addr;    /* my address information */
  struct sockaddr_in their_addr; /* connector's address information */
  socklen_t sin_size;
    
  /* Get port number for server to listen on */
  if (argc > 1) {
    port = htons(atoi(argv[1]));
    printf("Server is using port %d\n", ntohs(port));
  }
  else {
    port = htons(12345);
    printf("Server is using port %d\n", ntohs(port));
  }

  /* generate the socket */
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket");
    exit(1);
  }

  /* generate the end point */
  my_addr.sin_family = AF_INET;         /* host byte order */
  my_addr.sin_port = port;     /* short, network byte order */
  my_addr.sin_addr.s_addr = INADDR_ANY; /* auto-fill with my IP */

  /* To allow quick reuse of port after C-c */
  int reuse = 1;
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(reuse)) < 0)
    perror("setsockopt(SO_REUSEADDR) failed");

#ifdef SO_REUSEPORT
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, (const char*)&reuse, sizeof(reuse)) < 0) 
    perror("setsockopt(SO_REUSEPORT) failed");
#endif
    
  /* bind the socket to the end point */
  if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) \
      == -1) {
    perror("bind");
    exit(1);
  }

  /* start listnening */
  if (listen(sockfd, BACKLOG) == -1) {
    perror("listen");
    exit(1);
  }



  int thr_id[NUM_HANDLER_THREADS]; /* thread IDs */
  pthread_t  p_threads[NUM_HANDLER_THREADS]; /* thread structures   */
    
  /* create the request-handling threads */
  for (int i=0; i<NUM_HANDLER_THREADS; i++) {
    thr_id[i] = i;
    pthread_create(&p_threads[i], NULL, handle_requests_loop, (void*)&thr_id[i]);
  }

  printf("Server starts listening ...\n");

  /* repeat: accept, send, close the connection */
  /* for every accepted connection, use a separate process or thread to serve it */
  /* main accept() loop */
  while(1) { 
    sin_size = sizeof(struct sockaddr_in);
    if ((new_fd = accept(sockfd, (struct sockaddr *)&their_addr, \
			 &sin_size)) == -1) {
      perror("Failed to accept connection");
      continue;
    }
    printf("server: got connection from %s\n", inet_ntoa(their_addr.sin_addr));
    add_request(new_fd, &request_mutex, &got_request);
  }
  close(sockfd);
  return 0;
}
