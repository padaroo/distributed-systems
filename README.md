# BSD Socket Minesweeper Game

Designed for a forth year elective on Linux systems programming (in C). Client-server architecture, linked lists with bubble sort algorithm, process synchronisation, and multithreading developed in this project. Ncurses was implemented for a clean UI. Final code and report found in the submission folder.