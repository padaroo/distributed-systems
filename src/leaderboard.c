#include "../inc/leaderboard.h"

void swap(LIST_NODE_T *a, LIST_NODE_T *b) {
  NODE_PLAYER_T temp = a->payload;
  a->payload = b->payload;
  b->payload = temp;
}

void bubble_sort(LIST_NODE_T *head) {
  int swapped;
  LIST_NODE_T *ptrl;
  LIST_NODE_T *lptr = NULL;

  /* Checking for empty list */
  if (head == NULL)
    return;

  do {
    swapped = 0;
    ptrl = head;


    while (ptrl->next != lptr)
      {
	/* Rank by seconds taken */
	if (ptrl->payload.time > ptrl->next->payload.time) {

	  swap(ptrl, ptrl->next);
	  swapped = 1;
	}
	
	/* If two or more games have the same number of seconds then the game played by the player */
	/* with the highest number of games won should be displayed last. */
	else if(ptrl->payload.time == ptrl->next->payload.time) {
	  if (ptrl->payload.num_games_played > ptrl->next->payload.num_games_played) {

	    swap(ptrl, ptrl->next);
	    swapped = 1;
	  }
	}
	ptrl = ptrl->next;
      }
    lptr = ptrl;
  }
  while (swapped);
}

/*  ** Order the node by name
 */

int OrderNodeByName(
			       LIST_NODE_T *I__head,
			       const char  *I__name,
			       LIST_NODE_T **_O_parent
			       )
{
  int rCode=0;
  LIST_NODE_T *parent = NULL;
  LIST_NODE_T *curNode = I__head;

  /* Inform the caller of an 'empty list' condition. */
  if(NULL == I__head)
    {
      rCode=ENOENT;
      goto CLEANUP;
    }

  /* Find a node with a payload->name string greater than the I__name string */
  while(curNode)
    {
      if(strcmp(curNode->payload.name, I__name) > 0)
	break;

      parent = curNode; /* Remember this node. It is the parent of the next node. */
      curNode=curNode->next;  /* On to the next node. */
    }

  /* Set the caller's 'parent' pointer. */
  if(_O_parent)
    *_O_parent = parent;

 CLEANUP:

  return(rCode);
}

/*
 ** Allocate, initialize, and insert a new node in an ordered list.
 */
int Insert_Node(
			  LIST_NODE_T **IO_head,
			  char         *I__name,
			  int I__time,
			  int I__won,
			  int I__num_games_played)
{
  int rCode=0;
  LIST_NODE_T *parent;
  LIST_NODE_T *newNode = NULL;

  /* Allocate memory for new node (with its payload). */
  newNode=malloc(sizeof(*newNode));
  if(NULL == newNode)
    {
      rCode=ENOMEM;   /* ENOMEM is defined in errno.h */
      fprintf(stderr, "malloc() failed.\n");
      goto CLEANUP;
    }

  /* Initialize the new node's payload. */
  snprintf(newNode->payload.name, sizeof(newNode->payload.name), "%s", I__name);
  newNode->payload.won = I__won;
  newNode->payload.num_games_played = I__num_games_played;
  newNode->payload.time = I__time;

  /* Alphabetical Ordering of this node */
  rCode= OrderNodeByName(*IO_head, I__name, &parent);
  switch(rCode)
    {
    case 0:
      break;

    case ENOENT:
      /* Handle empty list condition */
      newNode->next = NULL;
      *IO_head = newNode;
      rCode=0;
      goto CLEANUP;

    default:
      fprintf(stderr, "OrderNodeByName() reports: %d\n", rCode);
      goto CLEANUP;
    }

  /* Handle the case where all current list nodes are greater than the new node. */
  /* (Where the new node will become the new list head.) */
  if(NULL == parent)
    {
      newNode->next = *IO_head;
      *IO_head = newNode;
      goto CLEANUP;
    }

  /* Final case, insert the new node just after the parent node. */
  newNode->next = parent->next;
  parent->next = newNode;

 CLEANUP:

  return(rCode);
}

