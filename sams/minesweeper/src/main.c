/* #include <stdio.h>  */
#include <stdlib.h>
#include <ncurses.h> /* ncurses.h includes stdio.h */
#include <string.h>
#include <time.h>

#define RANDOM_NUMBER_SEED 42
#define NUM_TILES_X 9 + 2
#define NUM_TILES_Y 9 + 2
#define NUM_MINES 10
#define CURSOR_OFFSET 2

/* Information for each tile */
typedef struct
{
  char symbol; /* '-' = unknown, '*' = mine, 'o' = no mine */
  int adjacent_mines;
  bool revealed;
  bool is_mine;
  char x_coord;
  int y_coord;
  bool exists; 			/* False . This will be for padding the array for boundary conditions */
} Tile;

/* store game state in a struct */
typedef struct GameState
{
  // ... additional fields ...
  Tile tiles[NUM_TILES_X][NUM_TILES_Y]; // 2D array of tiles, each with adj, revealed?, is_mine? members
  int mines_remaining;
  bool running;
  int cursor_x;
  int cursor_y;
  WINDOW t_window;
  int mine_counter;
} Mine_Yard;

/* Function protoypes */
int menu();

/* init the number of adjacent mines*/
void init_adjacent_mines (Mine_Yard *minesweeper){
    for(int i = 0; i<NUM_TILES_X; i++){
        for(int j = 0; j<NUM_TILES_Y; j++){
            for(int x = i-1; x<i+2;x++){
                for(int y = j-1; y<j+2;y++){
                    if(minesweeper->tiles[x][y].is_mine==true){
                        minesweeper->tiles[i][j].adjacent_mines = minesweeper->tiles[i][j].adjacent_mines +1;
                    }
                }
            }
        }
    }
}

/* resets all tiles to unknown and then populates 10 mines */
void clear_tiles(Mine_Yard *minesweeper)
{
  int r, c;
  //char list[NUM_TILES_X] = "ABCDEFGHI";

  /* set inital variables to all tiles */
  for (r = 0; r < NUM_TILES_Y; r++)
    {
      for (c = 0 ; c < NUM_TILES_X; c++)
	{
	  if (r == 0 || r >= NUM_TILES_Y-1 || c == 0 || c >= NUM_TILES_X-1) {
	  minesweeper->tiles[r][c].symbol = '.';
	  minesweeper->tiles[r][c].revealed = FALSE;
	  minesweeper->tiles[r][c].is_mine = FALSE;
	  minesweeper->tiles[r][c].exists = FALSE;
	  minesweeper->tiles[r][c].y_coord = r;
	  minesweeper->tiles[r][c].x_coord = c;
	  }
	  else {
	  minesweeper->tiles[r][c].symbol = '-';
	  minesweeper->tiles[r][c].is_mine = FALSE;
	  minesweeper->tiles[r][c].revealed = FALSE;
	  minesweeper->tiles[r][c].exists = TRUE;
	  minesweeper->tiles[r][c].y_coord = r;
	  minesweeper->tiles[r][c].x_coord = c;
	  }
	}
    }
}

/* resets all tiles to unknown and then populates 10 mines */
void init_tiles(Mine_Yard *minesweeper)
{
  int r, c;
  //char list[NUM_TILES_X] = "ABCDEFGHI";
  init_adjacent_mines(minesweeper); 
  /* set inital variables to all tiles */
  for (r = 0; r < NUM_TILES_Y; r++)
    {
      for (c = 0 ; c < NUM_TILES_X; c++)
	{
	  if (r == 0 || r >= NUM_TILES_Y-1 || c == 0 || c >= NUM_TILES_X-1) {
	  minesweeper->tiles[r][c].symbol = '.';
	  minesweeper->tiles[r][c].revealed = FALSE;
	  minesweeper->tiles[r][c].is_mine = FALSE;
	  minesweeper->tiles[r][c].exists = FALSE;
	  minesweeper->tiles[r][c].y_coord = r;
	  minesweeper->tiles[r][c].x_coord = c;
	  }
	  else {
	  minesweeper->tiles[r][c].symbol = '-';
	  minesweeper->tiles[r][c].is_mine = FALSE;
	  minesweeper->tiles[r][c].revealed = FALSE;
	  minesweeper->tiles[r][c].exists = TRUE;
	  minesweeper->tiles[r][c].y_coord = r;
	  minesweeper->tiles[r][c].x_coord = c;
	  }
	}
    }
}

void add_mines(Mine_Yard *minesweeper)
{
  minesweeper->mines_remaining = NUM_MINES;
  minesweeper->mine_counter = 0;

  /* add 10 mines randomly */
  int i;
  for (i = 0; i < NUM_MINES; i++)
    {
      int x, y;
      do
	{
	  x = (rand() % NUM_TILES_X-1);
	  y = (rand() % NUM_TILES_Y-1);
	}
      /* } while (tile_contains_mine(x,y)); */
      while (minesweeper->tiles[y][x].is_mine || !minesweeper->tiles[y][x].exists);
      /* place mine at x,y */
      minesweeper->tiles[y][x].symbol = '*';
      minesweeper->tiles[y][x].is_mine = TRUE;
      minesweeper->mine_counter++;
    }
}

/* Shows all mines - useful for debugging */
void show_all(Mine_Yard *obj, WINDOW *window)
{
  int r, c;

  for (r = 0; r < NUM_TILES_Y; r++)
    {
      mvwprintw(window, r + 2, 1, "%d ", r );
      mvwprintw(window, 1, r+2, "%d ", r );
      for (c = 0; c < NUM_TILES_X; c++)
	{
	  //mvwprintw(window, c + 2, 0, "%d ", c); /* print the columns */
	  mvwprintw(window, r + 2, c + 2, "%c", obj->tiles[r][c].symbol);
	}
    }
  wnoutrefresh(window);
  refresh();
}

/* Will print the mine Y,X coordinates out in the debug window */
void list_mine_coords(Mine_Yard *game, WINDOW *debug_window)//, WINDOW *window)
{
  int r, c;
  wclear(debug_window);
  wprintw(debug_window, "DEBUG WINDOW\n\n");
  for (r = 0; r < NUM_TILES_Y; r++)
    {
      for (c = 0; c < NUM_TILES_X; c++)
	{
	  if (game->tiles[r][c].is_mine)
	    {
	      wprintw(debug_window, "%d\t", game->tiles[r][c].y_coord); //,  obj->tiles[r][c].y_coord);
	      wprintw(debug_window, "%d\n", game->tiles[r][c].x_coord);
	    }
	}
      refresh();
      wrefresh(debug_window);
    }
}

/* Draw the and update the minesweeper area. */
/* What the user will see for the minesweeper area*/
void show_tiles(Mine_Yard *game,  WINDOW *window){
  /* Draw the minefield and coordinates */
  int r, c;
  char unknown;

  unknown = '-';
  for (r = 1; r < NUM_TILES_Y-1; r++) {
    mvwprintw(window, r + 2, 1, "%d ", r );
    mvwprintw(window, 1, r+2, "%d ", r );
    for (c = 1; c < NUM_TILES_X-1; c++)
      {
	if (game->tiles[r][c].revealed)
	  {
	    mvwprintw(window, r + 2, c + 2, "%c", game->tiles[r][c].symbol);
	  }
	else
	  {
	    mvwprintw(window, r + 2, c + 2, "%c", unknown);
	  }
      }
  }
  //refresh();
  wnoutrefresh(window);
}

void show_tiles_true(Mine_Yard *game,  WINDOW *window){
  /* Draw the minefield and coordinates */
  int r, c;
  char unknown;

  unknown = '-';
  for (r = 0; r < NUM_TILES_Y; r++) {
    mvwprintw(window, r + 2, 0, "%d ", r );
    mvwprintw(window, 1, r+2, "%d ", r );
    for (c = 0; c < NUM_TILES_X; c++)
      {
	if (game->tiles[r][c].exists) {
	  if (game->tiles[r][c].revealed)
	    {
	      mvwprintw(window, r + 2, c + 2, "%c", game->tiles[r][c].symbol);
	    }
	  else
	    {
	      mvwprintw(window, r + 2, c + 2, "%c", unknown);
	    }
	  
	}
	else {mvwprintw(window, r+2, c+2, "%c", game->tiles[r][c].symbol);}
      }
  }
  //refresh();
  wnoutrefresh(window);
}

/* GAME OVER: TO FINISH:
   will save the scores and return to menu */
void game_over() {
  clear();
  printw("GAME OVER");
  refresh();
  menu();
}

int menu() {
  clear();
  refresh();

  attron(COLOR_PAIR(1));
  attron(A_BOLD);
  attron(A_UNDERLINE);
  attron(A_STANDOUT);
  printw("Welcome to the Minesweeper gaming system\n\n");
  attroff(A_BOLD);
  attroff(A_UNDERLINE);
  attroff(A_STANDOUT);
  attroff(COLOR_PAIR(1));

  printw("Please enter a selection: \n");
  printw("<1> Play Minesweeper\n");
  printw("<2> Show Leaderboard\n");
  printw("<3> Quit\n\n");
  printw("Selection option (1-3): ");
  refresh();

  
  bool running = TRUE;
  int ch;
  while (running) {
    ch = wgetch(stdscr);

    switch (ch) {
    case '3': printw("Goodbye\n");
      //game->running = FALSE;
      running = FALSE;
      return 0;
    case '2': printw("Leaderboard loading...\n");
      // Call Leader board - probably a struct within struct like Tiles

      break;
    case '1': printw("Setting up Minesweeper\n");
      return 1;
      
    }
  }
  return 0;
}

void open_tile(Mine_Yard *game, WINDOW *win, WINDOW *debug)
{
  int x = game->cursor_x - CURSOR_OFFSET;
  int y = game->cursor_y - CURSOR_OFFSET;
  mvwprintw(debug, getmaxy(debug)-1, 0, "Cursor: %d %d ", y, x);
  wrefresh(debug);
  if (game->tiles[y][x].is_mine)
    {
      game->running = FALSE;
      //game_over();
    }

  else if (!game->tiles[y][x].revealed)
    {
      game->tiles[y][x].symbol = 'o';
      game->tiles[y][x].revealed = TRUE;
      
      //function to call:
      /* 
	 0. Set tile to open (either 0 or o or .)
	 1. Set tile->revealed to True
	 2. for each adj. tile L,R,U,D:
	 2.1 check adj. tiles to LRUD. 
	 2.2 for each adj. tiles that are mines, increment distance counter, and use that as symbol (atoi or sprintf function will cast int->char)
	 2.3 if no adj. tiles are mines, recursive(open tile[adj tile][adj-tile]


      */

      wrefresh(win);
      wrefresh(debug);
    }

	 /* 2.3 if no adj. tiles are mines, recursive(open tile[adj tile][adj-tile]*\/ */
    
  /* obj->tiles[y][x].revealed =true; */
  /* sprinf(obj->tiles[y][x].adjacent_mines); /\*not sure about how to print the int at the right place*\/ */
  /* if(obj->tiles[y][x].adjacent_mines==0){ */
  /*   for(int i = y-1; i<y+2;i++){ */
  /*     for(int j = x-1; j<x+2;j++){ */
  /*         open_tile(obj,win,debug,i,j); */
  /*     } */
  /*   } */
  /* } */
    
    wrefresh(win);
    wrefresh(debug);
 

}

/* User Interface initialization */
void init_ncurses() {
  initscr(); // initialize curses
  curs_set(2);
  cbreak();             // pass key presses to program, but not signals
  noecho();             // don't echo key presses to screen
  keypad(stdscr, TRUE); // allow arrow keys
  timeout(500);         // no blocking on getch()
  start_color();        /* Start color 			*/
  init_pair(1, COLOR_RED, COLOR_BLACK);
  init_pair(2, COLOR_GREEN, COLOR_BLACK);
  init_pair(3, COLOR_CYAN, COLOR_BLACK);
  init_pair(4, COLOR_WHITE, COLOR_BLACK);
  init_pair(5, COLOR_WHITE, COLOR_BLUE);
  bkgd(COLOR_PAIR(5));
}

void render_game(Mine_Yard *game, WINDOW *info_window, WINDOW *game_window, time_t seconds) {

  wclear(info_window);
    
  //  show_all(game, game_window); // This will show mines graphically
  show_tiles(game, game_window);
  show_tiles_true(game, &game->t_window);
  mvwprintw(info_window, 2, 1, "Number of mines to place : %d", game->mines_remaining);
  mvwprintw(info_window, 4, 1, "Cursor Position (y,x): %d %d", game->cursor_y-CURSOR_OFFSET, game->cursor_x-CURSOR_OFFSET);
  mvwprintw(info_window, 6, 1, "Time taken: %ld", seconds);
  mvwprintw(info_window, 8, 1, "Press q to quit");
  mvwprintw(stdscr, 0, (getmaxx(stdscr)) / 2, "MINESWEEPER"); // Screen Title
  refresh();
  box(info_window, 0, 0);
  //wrefresh(stdscr);
  wrefresh(info_window);
  wrefresh(game_window);
  wrefresh(&game->t_window);
  wmove(game_window, game->cursor_y, game->cursor_x);
  wrefresh(game_window);
}

int game() {
  
  Mine_Yard game;

/* Setup Windows */
  WINDOW *game_window, *info_window, *debug_window, *true_window;
  int main_x, main_y;
  getmaxyx(stdscr, main_y, main_x);
  game_window = newwin(main_y/3, main_x/4, 2, 2);
  info_window = newwin(main_y/4, main_x, main_y - getmaxy(game_window), 0);  
  debug_window = newwin(main_y / 3, main_x/4, 2, main_x / 3);
  true_window =  newwin(main_y / 3,  main_x/4, 2, 2 *(main_x / 3));
  box(info_window, 0, 0); 
  box(game_window, 0, 0); 
 
  game.running = TRUE;

  clear();  
  init_tiles(&game); // 9x9 grid marked as unknown
  add_mines(&game); //randomly add 10 mines
  list_mine_coords(&game, debug_window); // show all mine coordinates on debug window
  game.t_window = *true_window;
  box(&game.t_window, 0, 0);

  int ch;
  time_t seconds;
  time_t started = time(NULL);
  

  /* Get cursor position information */
  game.cursor_x = 3; game.cursor_y = 3;
  wmove(game_window, game.cursor_y, game.cursor_x);
  getyx(game_window, game.cursor_y, game.cursor_x);

  while (game.running)
    {
    
      /* GAME LOOP: Process Input and Update Game State*/
      ch = getch();
      switch (ch)
	{
	case KEY_DOWN:
	  if (game.cursor_y == NUM_TILES_Y) {
	    continue;
	  }
	  else {
	  game.cursor_y++;
	  }
	  break;
	case KEY_UP:
	  if (game.cursor_y == 3) {
	    continue;
	  }
	  else {
	    game.cursor_y--;
	  }
	  break;
	case KEY_LEFT:
	  if (game.cursor_x == 3){
	    continue;
	  }
	  else {
	  game.cursor_x--;
	  }
	  break;
	case KEY_RIGHT:
	  if (game.cursor_x == NUM_TILES_X) {
	    continue;
	  }
	    else {
	      game.cursor_x++;
	    }
	  break;
	case 'b':
	  open_tile(&game, game_window, debug_window);
	  break;
	case 'f':
	  // flag_tile();
	  break;
	case 'q':
	  endwin();
	  game.running = FALSE;
	}

      /* Update timer */
      seconds = difftime(time(NULL), started);

      /* Render Game */

      render_game(&game, info_window, game_window, seconds);

    }

  return 0;
  
}

int main(int argc, char *argv[])
{
  /* Initialisations */
  srand(RANDOM_NUMBER_SEED);
  init_ncurses();
  clear();

  /* Main computational loop */
  int ch;
  while(1) {
  ch = menu();
  if (ch == 0) {
    break;
  }
  else if (ch == 2) {
    // load leader board
  }
  else {
  game();
  // TODO : after game finishes save name and score
   }
  }
  
  endwin();
  return 0;
}
