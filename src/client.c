#include "../inc/client.h"

/* Ncurses User Interface initialization */
void init_ncurses() {  
  initscr(); // initialize curses screens
  curs_set(2);
  cbreak();             // pass key presses to program, but not signals
  keypad(stdscr, TRUE); // allow arrow keys
  start_color();
  /* Some colour properties */
  init_pair(1, COLOR_RED, COLOR_BLACK);
  init_pair(2, COLOR_GREEN, COLOR_BLACK);
  init_pair(3, COLOR_CYAN, COLOR_BLACK);
  init_pair(4, COLOR_WHITE, COLOR_BLACK);
  init_pair(5, COLOR_WHITE, COLOR_BLUE);
  bkgd(COLOR_PAIR(5)); 		/* Blue Background */
  clear();
}

/* Sends an int or an array of ints over the network */
void Send_Array_Data(int socket_id, int *myArray) {
  int i=0;
  uint16_t statistics;  
  for (i = 0; i < ARRAY_SIZE; i++) {
    statistics = htons(myArray[i]);
    send(socket_id, &statistics, sizeof(uint16_t), 0);
  }
}


/* Draw the and update the minesweeper area. */
/* What the user will see for the minesweeper area*/
void show_tiles(Mine_Yard *game,  WINDOW *window){
  /* Draw the minefield and coordinates */
  int r, c;
  int offset = 2;

  for (r = 1; r < NUM_TILES_Y-1; r++) {
    mvwprintw(window, r + offset, 1, "%d ", r );
    mvwprintw(window, 1, r + offset, "%d ", r );
    for (c = 1; c < NUM_TILES_X-1; c++)
      {
	mvwprintw(window, r + offset, c + offset, "%c", game->tiles[r][c]);
      }
  }
  wnoutrefresh(window);
}

/* This continuously updates the display and redraws the screen */
void render_game(Mine_Yard *game, WINDOW *info_window, WINDOW *game_window) {

  wclear(info_window);
  box(info_window, 0, 0);
  show_tiles(game, game_window);

  /* Winning Screen */
  if (game->mines_remaining == 0) {
    box(info_window, 0,0);
    mvwprintw(info_window, 2, 1, "YOU ARE A WINNER!");
    mvwprintw(info_window, 4, 1, "Press any key to continue");
    wrefresh(info_window);
    wmove(game_window, game->cursor_y, game->cursor_x);
    wrefresh(game_window);
    wrefresh(info_window);
    refresh();
    wgetch(info_window);
  }

  /* Losing Screen */
  else if ( game->over == true ) {
    box(info_window, 0,0);
    mvwprintw(info_window, 2, 1, "YOU ARE A LOSER!");
    mvwprintw(info_window, 4, 1, "Press any key to continue");
    wrefresh(info_window);
    wmove(game_window, game->cursor_y, game->cursor_x);
    wrefresh(game_window);
  }

  /* Game play screen */
  else {
    mvwprintw(info_window, 2, 1, "There is %s mine here", game->a_mine);
    mvwprintw(info_window, 4, 1, "Number of mines left to place : %d", game->mines_remaining);
    mvwprintw(info_window, 6, 1, "Press p to place a flag");
    mvwprintw(info_window, 8, 1, "Press r to reveal tile");
    mvwprintw(info_window, 10, 1, "Press q to quit");
    wrefresh(info_window);
    wmove(game_window, game->cursor_y, game->cursor_x);
    wrefresh(game_window);
  }

}

/* Shows all the tiles - used when its the end of a game */
void final_tiles(int socket_id, Mine_Yard *game,  WINDOW *game_window){
  int n;

  //Receive and Display new mineyard
  for (int i = 0; i < NUM_TILES_X; i++) {
    for (int j = 0; j < NUM_TILES_Y; j++) {
      n= read(socket_id, &game->tiles[i][j],1); 
      if (n < 0) perror("ERROR writing to socket");	
    }
  }
  show_tiles(game, game_window);
}

/* Receives an int or an array of ints from the network */
int* Receive_Array_Int_Data(int socket_identifier, int size) {
  int number_of_bytes;
  int i = 0;
  uint16_t statistics;
	
  int *results = malloc(sizeof(int)*size);
  for (i=0; i < size; i++) {
    if ((number_of_bytes=recv(socket_identifier, &statistics, sizeof(uint16_t), 0))
	== RETURNED_ERROR) {
      perror("recv");
      exit(EXIT_FAILURE);					    
    }
    results[i] = ntohs(statistics);
  }
  return results;
}


//Receive and print the leaderboard array 
void display_lb(int socket_id){

  timeout(-1);			/* Changes keypress halt to blocking */
  
  /* Leaderboard size */
  int *n = Receive_Array_Int_Data(socket_id,  ARRAY_SIZE);

  clear();
  printw("LEADERBOARD\n");

  printw(  "\n-------------------------------------------------------------------\n");
  if (*n == 0) {
    printw("There is no information stored in the leaderboard - try again later");
    printw("\n-------------------------------------------------------------------\n");
  }
  else {
    for (int i = 0; i < *n; i++)
      {
	char name[ARRAY_SIZE] = {0};
	read(socket_id, name, ARRAY_SIZE);
	int *time = Receive_Array_Int_Data(socket_id,  ARRAY_SIZE);
	int *won =  Receive_Array_Int_Data(socket_id,  ARRAY_SIZE);
	int *played =  Receive_Array_Int_Data(socket_id,  ARRAY_SIZE);

	printw("Name = %s, Time = %d seconds, Won = %d games won, %d of games played\n", name, *time, *won, *played);
      } printw("\n----------------------------------------------------------------------\n");
  }
  
  refresh();
  printw("\n\npress any key to continue");
  refresh();
  getch();
  main_menu(socket_id);
}

//Initialize the mineyard with all tiles unknown
void init_mines(Mine_Yard *game){
  game->mines_remaining=10;
  game->running=true;
  game->mine_counter=0;
  for (int i = 0; i < NUM_TILES_X; i++) {
    for (int j = 0; j < NUM_TILES_Y; j++) {
      game->tiles[i][j] = '-';
    }
  }
}

/* When 'r' is pressed: reveal_tile asks server for 
position information and updates the client */
int reveal_tile(int socket_id, Mine_Yard *game){
  //Send x and y positions found at cursor location
  Send_Array_Data(socket_id, &game->cursor_x);
  Send_Array_Data(socket_id, &game->cursor_y);

  //Receive if there is a mine here or not	
  char ans;
  int n;
  n= read(socket_id, &ans,1);
  if (n < 0) perror("ERROR reading from socket");

  /* If there's a mine: exit flag is true */
  if(ans == 'y'){
    
    game->a_mine = "a";
    return 1;    
  }

  else if(ans == 'n'){
    game->a_mine = "no";
  }


  //Receive and Display new mineyard
  for (int i = 0; i < NUM_TILES_X; i++) {
    for (int j = 0; j < NUM_TILES_Y; j++) {
      n= read(socket_id, &game->tiles[i][j],1);	
      
      if (n < 0) perror("ERROR writing to socket");	
    }
  }
  return 0;
}

/* Used when a player presses 'p': checks server is there 
is a flag found at that cursor position */
void place_flag(int socket_id, Mine_Yard *game){
  //Send x and y	
  Send_Array_Data(socket_id, &game->cursor_x);
  Send_Array_Data(socket_id, &game->cursor_y);

  //Receive information if a mine is there or not and display a message about it
  char ans;
  int n;

  n= read(socket_id, &ans,1);
  if (n < 0) perror("ERROR reading from socket");

  /* IF there is a mine there (and flag is placed)
mines remaining is reduced */
  if(ans == 'y'){
    game->a_mine = "a";
    game->mines_remaining --;
  }

  /* In a true game, the player would not recieve this information
which indicates there is no mine at that location */
  else if(ans == 'n'){
    game->a_mine = "no";
  }

  //Receive and Display new mineyard
  for (int i = 0; i < NUM_TILES_X; i++) {
    for (int j = 0; j < NUM_TILES_Y; j++) {
      n= read(socket_id, &game->tiles[i][j],1); 
      if (n < 0) perror("ERROR writing to socket");	
    }
  }
}


/* Start a new game, initalise the mines, windows, etc
and go into main game loop to get users input to open tiles, 
reveal flags, or quit the game */
int run_game(int socket_id) {
  Mine_Yard game;
  init_mines(&game);	//initialize the mineyard with all tiles unknown
    
  /* Setup Windows */
  noecho();			     /* Stops print out to screen */
  WINDOW *game_window;
  WINDOW *info_window;

  /* Set Game Screen window dimensions */
  game_window = newwin(NUM_TILES_Y+2, NUM_TILES_X+3, 2, 2);

  /* Get game window dimensions */
  int gamew_x;
  gamew_x = getmaxx(game_window);
  info_window = newwin(NUM_TILES_Y+2, 40 , 2 , gamew_x + 2);

  /* Draw pretty boxes */
  box(info_window, 0, 0); 
  box(game_window, 0, 0);

  /* Players choice used in switch statement */
  int ch;

  /* Get cursor position information */
  game.cursor_x = 3; game.cursor_y = 3;
  wmove(game_window, game.cursor_y, game.cursor_x);
  getyx(game_window, game.cursor_y, game.cursor_x);

  /* Clear the screen */
  clear();

  /* Minesweeper logo */
  mvwprintw(stdscr, 0, ((getmaxx(stdscr)) / 2) - (((getmaxx(stdscr))/4)/2), "MINESWEEPER"); // Screen Title

  timeout(50);         // no blocking on getch()

  game.a_mine = "?";
  game.over = false;
  game.running = true;

  int exit = 0; 		/* an exit flag */

  /* Start the main loop for user input to the game */
  for (;;)
    {
      /* GAME LOOP: Process Input and Update Game State*/
      ch = getch();
      switch (ch)
	{
	case KEY_DOWN:
	  if (game.cursor_y == NUM_TILES_Y) {
	    continue;
	  }
	  else {
	    game.cursor_y++;
	  }
	  break;

	case KEY_UP:
	  if (game.cursor_y == 3) {
	    continue;
	  }
	  else {
	    game.cursor_y--;
	  }
	  break;

	case KEY_LEFT:
	  if (game.cursor_x == 3){
	    continue;
	  }
	  else {
	    game.cursor_x--;
	  }
	  break;

	case KEY_RIGHT:
	  if (game.cursor_x == NUM_TILES_X) {
	    continue;
	  }
	  else {
	    game.cursor_x++;
	  }
	  break;

	case 'r': Send_Array_Data(socket_id, &ch);
	  exit = reveal_tile(socket_id, &game);
	  if (exit) {
	    game.over = true;
	    render_game(&game, info_window, game_window);
	    final_tiles(socket_id, &game, game_window);
	    refresh();
	    timeout(-1);
	    getch();
	    game.running = false;
	    return 1;
	  }
	  
	  break;

	case 't': Send_Array_Data(socket_id, &ch);
	  break;

	case 'p': Send_Array_Data(socket_id, &ch);
	  place_flag(socket_id, &game);
	  if(game.mines_remaining == 0){
	    final_tiles(socket_id,&game,game_window);
	    render_game(&game, info_window, game_window);
	    refresh();
	    timeout(-1);
	    game.running=false;
	    return 1;
	  } 
	  break;

	case 'q': Send_Array_Data(socket_id, &ch);
	  return 1;
	}

      /* Render Game 
       */
      render_game(&game, info_window, game_window);

    }
  
  //timeout(-1);
  return 0;
  
}

/* Tidies up the program  */
void shut_down(int socket_id) {
  close(socket_id);
  endwin();
  exit(EXIT_SUCCESS);
}

//main client request this function is called after the authentification
void main_menu(int socket_id){
  timeout(50);
  clear();

  attron(COLOR_PAIR(1)); attron(A_BOLD);
  attron(A_UNDERLINE); attron(A_STANDOUT);
  printw("Welcome to the Minesweeper gaming system\n\n");
  attroff(A_BOLD);  attroff(A_UNDERLINE);
  attroff(A_STANDOUT);  attroff(COLOR_PAIR(1));

  printw("Please enter a selection: \n");
  printw("<1> Play Minesweeper\n");
  printw("<2> Show Leaderboard\n");
  printw("<3> Quit\n\n");
  printw("Selection option (1-3): ");
  refresh();

  bool running = TRUE;
  int ch;

  while (running) {
    ch = wgetch(stdscr);
    /* the conversion below is needed to make a char from getch() into it's actual int (eg ascii '49' = symbol '1' but this is not an integer, rather the key pressed on keyboard, below makes '1' = int(1)*/
    int ia = ch - '0';
    Send_Array_Data(socket_id, &ia);
    switch (ch) {
      
    case '3': printw("\nGoodbye - Press any key to exit\n");
      running = FALSE;
      shut_down(socket_id);
      break;

    case '2': printw("Leaderboard loading...\n");
      display_lb(socket_id);
      break;

    case '1': printw("Setting up Minesweeper\n");
      run_game(socket_id);
      main_menu(socket_id);
      break;
    default :
      main_menu(socket_id);
	      
    }
  }
}

/* Initial authentication each client must proceed through
before playing the game */
void authenticate(int sockfd) {
  init_ncurses();
  
  WINDOW* main_win;

  /* Early test to make sure Ncurses will work on client system */
  if ( (main_win = initscr()) == NULL ) {
    fprintf(stderr, "Error initialising ncurses.\n");
    exit(EXIT_FAILURE);
  }

  attron(COLOR_PAIR(2)); attron(A_BOLD); attron(A_UNDERLINE); attron(A_STANDOUT);
  printw("AUTHENTICATION\n");
  attroff(A_BOLD); attroff(A_UNDERLINE); attroff(A_STANDOUT); attroff(COLOR_PAIR(2));

  char buffer[256];
  char ans = 'n';
	

  printw("Enter user name: ");
  getstr(buffer);
      
  /* Check for empty buffer being sent accidentally */
  while (buffer[0] == '\0') {
    getstr(buffer);
    refresh();
  }
    
  int n;
  /* send the user name to the server */
  n = write(sockfd, buffer, strlen(buffer));
  if (n < 0)
    printw("ERROR writing to socket\n");

  bzero(buffer, 256);
  printw("Enter Password: ");
  getstr(buffer);

  /* Check for empty buffer being sent accidentally */
  while (buffer[0] == '\0') {
    getstr(buffer);
    refresh();
  }
	    
  /* send the password to the server */
  n = write(sockfd, buffer, strlen(buffer));
  if (n < 0)
    printw("ERROR writing to socket");

  /* Read servers response for password authentication */
  n = read(sockfd, &ans, 1);
  if (ans == 'y') {
    printw("Authenticated - Game to continue");
    main_menu(sockfd);
  }
  else {
    clear();
    printw("Incorrect User name or Password - Disconnecting\n");
    refresh();	
    getch();			/* Lets user wait read message */
    shut_down(sockfd);
  }
}
  
int main(int argc, char *argv[]) {
  
  int sockfd;
  struct hostent *he;
  struct sockaddr_in their_addr; /* connector's address information*/

  if (argc != 3) {
    printf("Not enough arugments supplied. Usage: client_hostname port_number\n");
    printf("Example: ./client localhost 12345\n");
    exit(EXIT_FAILURE);
  }

  if ((he=gethostbyname(argv[1])) == NULL) {  /* get the host info */
    clear();
    printf("Not enough argument supplied. Enter localhost as client_hostname\n");
    exit(EXIT_FAILURE);
  }

  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    printf("Socket could not be created\n");
    exit(EXIT_FAILURE);
  }

  their_addr.sin_family = AF_INET;      /* host byte order */
  their_addr.sin_port = htons(atoi(argv[2]));    /* short, network byte order */
  their_addr.sin_addr = *((struct in_addr *)he->h_addr);
  bzero(&(their_addr.sin_zero), 8);     /* zero the rest of the struct */

  if (connect(sockfd, (struct sockaddr *)&their_addr, \
  	      sizeof(struct sockaddr)) == -1) {
    perror("connect");
    exit(1);
  }

  printf("awaiting server");

  /* Client and Server connected : time for authentication */
  authenticate(sockfd);

  /* Shut down below this line is somehow reached */
  endwin();
  close(sockfd);
  return 0;
}
