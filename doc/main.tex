\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{cite}
\usepackage{url}
\usepackage{hyperref}

\definecolor{mGreen}{rgb}{0,0.6,0}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\definecolor{mPurple}{rgb}{0.58,0,0.82}
\definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{CStyle}{
    backgroundcolor=\color{backgroundColour},   
    commentstyle=\color{mGreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{mGray},
    stringstyle=\color{mPurple},
    basicstyle=\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    language=C
}

\title{CAB403 Minesweeper Assignment}

\author{Pauline Grimault --- Samuel Zeter}

\date{\today}

\begin{document}
\maketitle


\section{Statement of Completeness} % indicating the tasks you attempted, any deviations from the assignment specification, and problems and deficiencies in your solution}
\label{sec:statement}

Tasks 1, 2, and 3 were completed. The program, on the client-side, runs using the \textit{ncurses} library, which allows a 'GUI-like' interface. During the game, instead typing in co-ordinates, users use the keypad arrows to move the cursor around the game to select appropriate tiles. Approval was given by Dr Timothy Chappell for the use of ncurses. It was included based on the fact that it is an industry standard for terminal programs, and provided a cleaner environment in which to play the game. A small deviation from the CRA is that the leader-board is listed in \textit{ascending} order. This is because ncurses allows the programmer to specify where text is output to the screen: this removes the problem of a scrolling screen of text, where it is prudent to sort the highest scores in descending order.\par

\section{Team Information}
Pauline Grimault, n10289259\\
Samuel Zeter, n5689406

\section{Statement of Contributions}
All work was equally shared by Pauline Grimault and Samuel Zeter.

\newpage

\section{Description of Mine-Yard Data Structure}
The code below is found in the \textit{server.h} header file, and is the Data Structure used for the Mine-Yard. An explanation is found below it.

\begin{lstlisting}[style=CStyle]

/* Information for each tile */
typedef struct
{
  char symbol; /* '-' = unknown, '*' = mine, 'o' = no mine */
  int adjacent_mines;
  bool revealed;
  bool is_mine;
  char x_coord;
  int y_coord;
  bool exists; 			/* False . This will be for padding the array for boundary conditions */
} Tile;

/* store game state in a struct */
typedef struct GameState
{
  // ... additional fields ...
  Tile tiles[NUM_TILES_X][NUM_TILES_Y]; // 2D array of tiles, each with adj, revealed?, is_mine? members
  int mines_remaining;
  bool running;
  int cursor_x;
  int cursor_y;
  int mine_counter;
} Mine__yard;


\end{lstlisting}

As seen in the code, a type defined struct called Mine\_Yard was created that includes cursor information, flags for the game to run, and counters. Within this struct is another struct named Tile. The purpose of tile is to hold symbol information, algorithm information (number of adjacent mines), and coordinate information of the tiles. Fitting a struct within a struct allows for, in this instance, a simple and concise way to hold all game state variables under one name in a block of memory: in this game, it was initialized as 'Mine\_Yard game'.

\section{Explanation of Leader-board Data Structure}

A singly-linked list was used for the leader-board data structure. This allowed for dynamic expansion (so that the leader-board is not constrained by a fixed number of entries), the ability to insert a node (a user's winning games) alphabetically, and the use of a simple sorting algorithm (Bubble Sort).\par
The use of linked-lists also provided a good learning opportunity, and a useful and thorough tutorial was found at stackoverflow [1]. Our solution was based on some of the tutorial code found here; however, it has been highly modified to suit our needs: indeed, the basic structures of linked lists are universal.\\
The linked-list implemented in our game works by inserting a node into a global linked list structure. This node consists of the player information (the payload), and a pointer to the next node in the list. This allows the list to be indefinitely extended (within reason of computing resources). Upon insertion of the node, it is given a position within the list. The position is dictated by the strings name, and arranges the node in alphabetical order through use of a simple swap procedure (the C Standard function 'Strncmp' is used to indicate the alphabetical ordering of the strings). Once a player requests a leader-board update, the list is 'bubble sorted'. This algorithm was chosen due to its simplicity. It works by comparing one node to the next, (in this case the player wins, times, and other ordering criteria), and if a node's time is lower, it swaps one with the other.

\section{Critical Section Problem}

As the leaderboard is a shared data-structure among clients, its writing to, and ordering needs to be protected, so that erroneous behaviour will not occur. This is done through mutex locks. When the leaderboard is shuffled via the bubble sort algorithm, nodes are moved, and thus memory is written to. To avoid multiple clients changing nodes, at the same time, a mutex lock is enabled every time the leaderboard is sorted. The sorting takes place only when the leaderboard is requested for display by the client ---this reduces server load. Initial insertion of a node (after a player has won), also requires a mutex lock, as it could potentially occur that a clients details are being written to a linked node, while another client is requesting a leaderboard update, or writing to the same memory location.\par
Mutex locks are also used in the multi-threaded implementation of the server. Set in the \textit{handle\_requests\_loop}, each thread that handles a client is locked until that client is released or disconnects.\par
The server also contains mutex locks when calling for random numbers. This is because the rand() function is not thread-safe, and as such, when mines are added (using the add\_mine function) the following code:

\begin{lstlisting}[style=CStyle]
	  /* Making random thread safe */
	  pthread_mutex_lock(&request_mutex);
	  x = (rand() % NUM_TILES_X-1);
	  y = (rand() % NUM_TILES_Y-1);
	  pthread_mutex_unlock(&request_mutex);
\end{lstlisting}
          
\section{Thread Pool Creation and Management}

Upon correct initialization of the server, ten threads are created using the code found below. Here, each new thread is started by the calling process, with the thread then invoking the handle\_requests\_loop argument. 


\begin{lstlisting}[style=CStyle]
 /* create the request-handling threads */
  for (int i=0; i<NUM_HANDLER_THREADS; i++) {
    thr_id[i] = i;
    pthread_create(&p_threads[i], NULL, handle_requests_loop, (void*)&thr_id[i]);
  }

  \end{lstlisting}

As there are no incoming connections, the server then continues, and awaiting an incoming client connection. If a correct connection results, the resulting socket identifier is then added to the linked list data structure, and its is then processed in handle\_requests\_loop argument, where the client is assigned a mutex locked thread, which calls the \textit{Client Request} function, which the proceeds with client authentication. Once the client disconnects, the thread returns back to the handle\_requests\_loop argument, and is released. This thread can now be used for new connections. Although 'printf' functions were kept minimal on the server, thread opening and closing comments will be displayed for ease of marking.

The maximum ten client connections is handled by the following code, where BACKLOG = 10. This solution means that clients that attempt a connection, when the BACKLOG is full, may have their service terminated until availability arises.

\begin{lstlisting}
  /* start listnening */
  if (listen(sockfd, BACKLOG) == -1) {
    perror("listen");
    exit(1);
  }

  \end{lstlisting}

\section{Compilation and Running of the Program}
\begin{enumerate}
\item As ncurses has been used, it may be necessary to install the required library files. This is easily done by the following commands: \\

  sudo apt install libncurses5-dev \\

\item Once this is complete, at the terminal, change director to 'src' and type 'make'. Both  client and server executable files will be created.

\item Type './server' for a default port of 12345 or './server port\_number' for a user specifier option, where 'port\_number' will be a registered port number in the range 1024–65535.

\item In a separate terminal window, type './client localhost port\_number', where port number is either the servers default or the chosen one.

  \item If code is to be changed and recompiled, type 'make clean' followed by make.

\end{enumerate}





\section{Bibliography}

[1] Moriancumer, Mahonri. \textit{Creating and Understanding linked lists of structs in C}. StackOverflow. 2018. URL: \url{https://stackoverflow.com/questions/23279119/creating-and-understanding-linked-lists-of-structs-in-c/23280743}}.


\end{document}
