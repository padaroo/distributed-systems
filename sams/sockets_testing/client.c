#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <unistd.h>
#include <pthread.h>

#include <ncurses.h>

	#define MAXDATASIZE 100 /* max number of bytes we can get at once */

	#define ARRAY_SIZE 30

	#define RETURNED_ERROR -1


void Receive_Array_Int_Data(int socket_identifier, int size) {
    int number_of_bytes, i=0;
    uint16_t statistics;

	int *results = malloc(sizeof(int)*ARRAY_SIZE);

	for (i=0; i < size; i++) {
		if ((number_of_bytes=recv(socket_identifier, &statistics, sizeof(uint16_t), 0))
		         == RETURNED_ERROR) {
			perror("recv");
			exit(EXIT_FAILURE);			
		    
		}
		results[i] = ntohs(statistics);
	}
	for (i=0; i < ARRAY_SIZE; i++) {
		printw("Array[%d] = %d\n", i, results[i]);
	}
}


/* User Interface initialization */
void init_ncurses() {
  initscr(); // initialize curses
  curs_set(2);
  cbreak();             // pass key presses to program, but not signals
  //noecho();             // don't echo key presses to screen
  keypad(stdscr, TRUE); // allow arrow keys
  //  timeout(500);         // no blocking on getch()
  start_color();        /* Start color 			*/
  init_pair(1, COLOR_RED, COLOR_BLACK);
  init_pair(2, COLOR_GREEN, COLOR_BLACK);
  init_pair(3, COLOR_CYAN, COLOR_BLACK);
  init_pair(4, COLOR_WHITE, COLOR_BLACK);
  init_pair(5, COLOR_WHITE, COLOR_BLUE);
  bkgd(COLOR_PAIR(5));
}

int send_int(int num, int fd)
{
    int32_t conv = htonl(num);
    char *data = (char*)&conv;
    int left = sizeof(conv);
    int rc;
    do {
        rc = write(fd, data, left);
        if (rc < 0) {
            if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
                // use select() or epoll() to wait for the socket to be writable again
            }
            else if (errno != EINTR) {
                return -1;
            }
        }
        else {
            data += rc;
            left -= rc;
        }
    }
    while (left > 0);
    return 0;
}

int receive_int(int *num, int fd)
{
    int32_t ret;
    char *data = (char*)&ret;
    int left = sizeof(ret);
    int rc;
    do {
        rc = read(fd, data, left);
        if (rc <= 0) { /* instead of ret */
            if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
                // use select() or epoll() to wait for the socket to be readable again
            }
            else if (errno != EINTR) {
                return -1;
            }
        }
        else {
            data += rc;
            left -= rc;
        }
    }
    while (left > 0);
    *num = ntohl(ret);
    return 0;
}

int main(int argc, char *argv[]) {
  init_ncurses();
	int sockfd, numbytes, i=0;  
	char buf[MAXDATASIZE];
	struct hostent *he;
	struct sockaddr_in their_addr; /* connector's address information */

	if (argc != 3) {
	  printw("usage: client_hostname port_number\n");
	  refresh();
	  endwin();
	  exit(1);
		
	}

	if ((he=gethostbyname(argv[1])) == NULL) {  /* get the host info */
	  printw("gethostbyname");
	  refresh();
	  endwin();
	  exit(1);
	}

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
	  printw("Socket");
	  refresh();
	  endwin();
	  exit(1);
	}


	their_addr.sin_family = AF_INET;      /* host byte order */
	their_addr.sin_port = htons(atoi(argv[2]));    /* short, network byte order */
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(their_addr.sin_zero), 8);     /* zero the rest of the struct */

	if (connect(sockfd, (struct sockaddr *)&their_addr, \
		    sizeof(struct sockaddr)) == -1) {
	  perror("connect");
	  exit(1);
	}
	bool authenticated = FALSE;
	char buffer[256];
	char ans = 'n';
	
	while (!authenticated) {
	  printw("Enter user name: ");
	  getstr(buffer);
	  refresh();


	  int n;
	  /* send the user name to the server */
	  n = write(sockfd, buffer, strlen(buffer));
	  if (n < 0) 
	    printw("ERROR writing to socket");

	  bzero(buffer, 256);
	  n = read(sockfd, &ans, 1);
	
	  printw("This was returned: %c\n", ans);

	  if (ans == 'y') {
	    printw("Enter Password: ");
	    getstr(buffer);
	    
	    /* send the password to the server */
	    n = write(sockfd, buffer, strlen(buffer));
	    if (n < 0) 
	      printw("ERROR writing to socket");
	    n = read(sockfd, &ans, 1);
	    if (ans == 'y') {
	      printw("Authenticated - Game to continue");
	    }
	    else {
	      printw("Incorrect Password");
	    }
	    
	    refresh();
	    getch();
	  }

	  else {
	    printw("Username incorrect, try again");
	    refresh();
	  }  
	  
	
	  refresh();
	}

	endwin();
	return 0;
}
