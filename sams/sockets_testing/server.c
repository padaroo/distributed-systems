/*
 *  Materials downloaded from the web. See relevant web sites listed on OLT
 *  Collected and modified for teaching purpose only by Jinglan Zhang, Aug. 2006
 */

#include <pthread.h>
#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>


#define ARRAY_SIZE 30  /* Size of array to receive */

#define BACKLOG 10     /* how many pending connections queue will hold */

#define RETURNED_ERROR -1


void Handle_Request(int socket_id) {

  int n;
  char buffer[256];
  char ans;

  for (;;) {
    bzero(buffer, 256); 	/* Clear the user_name buffer */


    /* Reads in the user name first */
    n = read(socket_id, buffer, 255);
    if (n < 0) perror("ERROR reading from socket");
    printf("\nMessage received: %s\n", buffer);

    //n = write(socket_id,"I got your message",18); /* To send statement back */
    if (n < 0) perror("ERROR writing to socket");
    printf("The user name given is %s\n", buffer);

    char line[100]; 		/* variable to read the file line by line */

    /* 1. Open Authentication file*/
    FILE *fp;
    fp=fopen("./Authentication.txt", "r");
    if (fp == NULL) {
      fprintf(stderr, "Could not open file, %s", strerror(errno));
    }

    char *user_name;
    char *password;
    char *saveptr;
  
    /* 2. Scan each line of file */
    while (fgets(line, sizeof(line), fp) != NULL) {

      /* 3. Check user name */
      user_name = strtok_r(line, "\t", &saveptr); 	/* Username */

      /* User name is found */
      if ((strcmp(buffer, user_name)) == 0) {
	ans = 'y';
	n = write(socket_id,&ans,1); /*ask client to send password */
	if (n < 0) perror("ERROR writing to socket");

	bzero(buffer, 256);
	/* Read in password to buffer */
	n = read(socket_id, buffer, 255);
	if (n < 0) perror("ERROR reading from socket");
	printf("The password given is %s\n", buffer);

	/* 4. Check Password */
	password = strtok_r(NULL, "\t", &saveptr); /* Password */
	int len;
	len = strlen(password);
	if( password[len-1] == '\n' )
	  password[len-1] = 0;

	/* 5. Password correct -> continue to game */
	if ((strcmp(buffer, password)) == 0) {
	  ans = 'y';	
	  n = write(socket_id,&ans,1);
	}
      }
      password = strtok_r(NULL, "\t", &saveptr); /* Password */
    }

    ans = 'n';	
    n = write(socket_id,&ans,1);

    /* 6. Close the authentication file */
    fclose(fp);
    /* printf("No username or matching password found"); */

  }

	      
}
  


int main(int argc, char *argv[]) {

  /* Thread and thread attributes */
  pthread_t client_thread;
  pthread_attr_t attr;


  int sockfd, new_fd;  /* listen on sock_fd, new connection on new_fd */
  struct sockaddr_in my_addr;    /* my address information */
  struct sockaddr_in their_addr; /* connector's address information */
  socklen_t sin_size;
  int i=0;

  /* Get port number for server to listen on */
  if (argc != 2) {
    fprintf(stderr,"usage: client port_number\n");
    exit(1);
  }

  /* generate the socket */
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket");
    exit(1);
  }

  /* generate the end point */
  my_addr.sin_family = AF_INET;         /* host byte order */
  my_addr.sin_port = htons(atoi(argv[1]));     /* short, network byte order */
  my_addr.sin_addr.s_addr = INADDR_ANY; /* auto-fill with my IP */
  /* bzero(&(my_addr.sin_zero), 8);   ZJL*/     /* zero the rest of the struct */

  /* bind the socket to the end point */
  if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) \
      == -1) {
    perror("bind");
    exit(1);
  }

  /* start listnening */
  if (listen(sockfd, BACKLOG) == -1) {
    perror("listen");
    exit(1);
  }

  printf("server starts listnening ...\n");

  /* repeat: accept, send, close the connection */
  /* for every accepted connection, use a sepetate process or thread to serve it */
  while(1) {  /* main accept() loop */
    sin_size = sizeof(struct sockaddr_in);
    if ((new_fd = accept(sockfd, (struct sockaddr *)&their_addr, \
			 &sin_size)) == -1) {
      perror("accept");
      continue;
    }
    printf("server: got connection from %s\n", \
	   inet_ntoa(their_addr.sin_addr));
    /* char buffer[256]; */
    /* int n; */
    /* bzero(buffer,256); */
    /*  n = read(sockfd,buffer,255); */
    /*  if (n < 0) perror("ERROR reading from socket"); */
     
    /*  printf("Here is the message: %s\n",buffer); */
 

    //Create a thread to accept client
				
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_create(&client_thread, &attr, Handle_Request, new_fd);

    pthread_join(client_thread,NULL);

    if (send(new_fd, "All of array data sent by server\n", 40 , 0) == -1)
      perror("send");

  }

  close(new_fd);  
}
