#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <err.h>
#include <string.h>

int main() {
  char line[100];
  char u_name[256] = "Paul"; 		/* This is the user name */
  char p_w[20] = "248273";
  
  /* 1. Open Authentication file*/
  FILE *fp;
  fp=fopen("./Authentication.txt", "r");
  if (fp == NULL) {
    fprintf(stderr, "Could not open file, %s", strerror(errno));
  }


  char *user_name;
  char *password;
  char *saveptr;
  
  /* 2. Scan each line of file */
  while (fgets(line, sizeof(line), fp) != NULL) {

    /* 3. Check user name */
    user_name = strtok_r(line, "\t", &saveptr); 	/* Username */

    if ((strcmp(u_name, user_name)) == 0) {

      /* 4. Check Password */
      password = strtok_r(NULL, "\t", &saveptr); /* Password */
      int len;
      len = strlen(password); 	/* Need this unfortuantely as newline character making parsing  */
      if( password[len-1] == '\n' )
	password[len-1] = 0;

      /* 5. Password correct -> continue to game */
      if ((strcmp(p_w, password)) == 0) {
	printf("Authenticated user name and password\n");
      }

      /* 5. Password incorrect -> return bad */
      else {
	printf("Username correct, password wrong\n");
      }

      }
      password = strtok_r(NULL, "\t", &saveptr); /* Password */
    }

  /* 6. Close the authentication file */
    fclose(fp);
    /* printf("No username or matching password found"); */

    return 0;
  }
